import argparse
from .mesh_all import main as mesh_all_main

def mesh_all():
    parser = argparse.ArgumentParser()
    parser.add_argument('seg_im')
    args = parser.parse_args()
    mesh_all_main(args.seg_im)
import os
import argparse
import numpy as np
import matplotlib.pyplot as plt
from skimage import measure
import pyacvd
import pyvista as pv
import tifffile as tf

from PIL import Image

from .converter_path import converter_path

def mesh_clustering(PolyData):
    """ reduces number of triangles in vtkPolyData object"""
    num_verts = 5000
    PData = pv.PolyData(PolyData)
    cobj = pyacvd.Clustering(PData)
    cobj.cluster(num_verts)  # new number of vertices 5000

    mesh = cobj.create_mesh()
    mesh.clean()

    cobj2 = pyacvd.Clustering(mesh)
    cobj2.cluster(num_verts)

    c = cobj2.create_mesh()

    # fix the normals

    # triangleCellNormals = vtk.vtkPolyDataNormals()
    # triangleCellNormals.SetInputData(c)
    # triangleCellNormals.ComputeCellNormalsOff()
    # triangleCellNormals.ComputePointNormalsOn()
    # triangleCellNormals.ConsistencyOn()
    # triangleCellNormals.AutoOrientNormalsOn()
    # triangleCellNormals.Update()

    c.clean()
    c.compute_normals(inplace=True)
    return c


def rgb2id_array(rgb_array):
    """Return identifier array from rgb array"""
    b = rgb_array[:, :, 2].astype(np.uint64)
    g = 256 * rgb_array[:, :, 1].astype(np.uint64)
    r = 256 * 256 * rgb_array[:, :, 0].astype(np.uint64)
    return r + g + b


def plot_contours(array, level=None):
    plt.imshow(array)
    for id in np.unique(array):
        contours = measure.find_contours(array, id)
        for n, contour in enumerate(contours):
            plt.plot(contour[:, 1], contour[:, 0], linewidth=2, color='black')


def extend_array(array, depth, cid):
    new_array = np.zeros([array.shape[0], array.shape[1], depth])
    bl = np.zeros([array.shape[0], array.shape[1], 1])

    for i in range(depth):
        new_array[np.where(array == cid)] = 1
    new_array = np.concatenate((bl, new_array, bl), axis=2)
    return new_array


def pick_cell(image):
    fig, ax = plt.subplots()
    plt.imshow(image, alpha=0.5)

    def onclick(event):
        global cell_id
        cell_id = image[int(event.ydata), int(event.xdata)]
        # print(cell_id)
        fig.canvas.mpl_disconnect(click_id)
        plt.close(1)

    click_id = fig.canvas.mpl_connect('button_press_event', onclick)
    plot_contours(image)
    plt.xlim([0, image.shape[1]])
    plt.ylim([image.shape[0], 0])
    plt.show()
    return cell_id


def marching_cubes_tvtk(array, depth, cid):
    array = extend_array(array, depth, cid)
    verts, faces, normals, values = measure.marching_cubes_lewiner(array, 0.5)
    threes = 3 * np.ones((faces.shape[0]), dtype=np.int)
    faces = np.c_[threes, faces]
    mesh = pv.PolyData(verts, np.hstack(faces))
    mesh.compute_normals(inplace=True)
    return mesh, array


def init_from_ply(ply_path):
    import subprocess
    ply_path = os.path.abspath(ply_path)
    conv_path = converter_path
    init_path = os.path.splitext(ply_path)[0] + ".init"
    command1 = "{} -input_format ply '{}' > '{}'".format(
        conv_path, ply_path, init_path)
    # print(command1)
    subprocess.call(command1, shell=True)
    appendpath = "append_cell_data.sh"
    subprocess.call("{} '{}'".format(appendpath, init_path), shell=True)


def tvtk_output_2d(im_path, output_dir, stl_output, cell_id):
    image = Image.open(im_path)
    array = np.asarray(image)
    if im_path.endswith(".png"):
        array = rgb2id_array(array)

    cell_id = int(cell_id)

    depth = 30

    mesh, array = marching_cubes_tvtk(array, depth, cell_id)
    mesh = mesh_clustering(mesh)


    (head, tail) = os.path.split(im_path)

    (fname, ext) = os.path.splitext(tail)

    fname = fname.replace(" ", "_") + "_{}".format(cell_id)

    if not output_dir:
        out_path = os.path.join(head, fname)
    else:
        out_path = os.path.join(output_dir, fname)

    mesh.save(out_path + '.vtk')
    mesh.save(out_path + '.ply')

    tf.imsave(out_path + '.tif', array.astype('float32'),
              imagej=True)

    # print("saved to : {}".format(out_path))

    init_from_ply(out_path + '.ply')

    return out_path + ".vtk"


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('im_path')
    parser.add_argument("-o", "--output_dir", default=None)
    parser.add_argument("-s", "--stl_output", action='store_true')
    parser.add_argument("-c", "--cell_id", default=False)
    args = parser.parse_args()

    tvtk_output_2d(args.im_path, args.output_dir,
                   args.stl_output, args.cell_id)

    # TODO check format of image
    # TODO check style of cell id encoding
